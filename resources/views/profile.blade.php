@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 row">
                <div class="col-md-6">
                    <div class="row p-0 mb-2">
                        <div class="col-md-4 text-14">
                            <span>Name:</span>
                        </div>
                        <div class="col-md-4 text-14">
                            <span>{{$profile_info->name}}</span>
                        </div>
                        <div class="col-md-4">
                        </div>
                    </div>
                    <div class="row p-0 mb-2">
                        <div class="col-md-4 text-14">
                            <span>Email:</span>
                        </div>
                        <div class="col-md-4 text-14">
                            <span>{{$profile_info->email}}</span>
                        </div>
                        <div class="col-md-4">
                        </div>
                    </div>
                    <div class="row p-0 mb-2">
                        <div class="col-md-4 text-14">
                            <span>Birthday:</span>
                        </div>
                        <div class="col-md-4 text-14">
                            <span>{{$profile_info->dob}}</span>
                        </div>
                        <div class="col-md-4">
                        </div>
                    </div>
                    <div class="row p-0 mb-2">
                        <div class="col-md-4 text-14">
                            <span>Gender:</span>
                        </div>
                        <div class="col-md-4 text-14">
                            @if($profile_info->gender == 1)
                                <span>Male</span>
                            @elseif($profile_info->gender == 2)
                                <span>Female</span>
                            @elseif($profile_info->gender == 3)
                                <span>Other</span>
                            @else 
                                <span></span>
                            @endif
                        </div> 
                        <div class="col-md-4">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row mb-2">
                        <div class="col-md-2 col-lg-3">
                        </div>
                        <div class="col-md-10 col-lg-9 p-0">
                            <div class="expandable">
                                <img src="{{Request::root()}}/assets/user/{{$profile_info->image}}" class="card-img" alt="..." style="height:200px; width:200px; object-fit: cover">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 col-lg-3">
                        </div>
                        <div class="col-md-10 col-lg-9 p-0">
                            <form class="mb-2" id="img_form" action="{{route('image-post')}}" method="post">
                                @csrf
                                <input type="file" id="img" name="img" />
                                <span class="text-danger" id="img_error" style="display: none">select image first</span>
                            </form>
                            <a id="submit" class="btn-success p-1">upload</a>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
@endsection

@section('custom_js')
<script>
    $(document).ready(function() {

        $("#submit").click(function(){
            //console.log("clicked");
            var flag = 0;

            if($('#img').val() == '' || $('#img').val() == null){
                flag = 1;
                $('#img_error').show();
            }

            if(flag == 0){
                if(confirm("Are you sure?")){
                    $("#img_form").submit();
                }
            }
        });

    });
</script>
@endsection