<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    public function likeFrom()
    {
        return $this->belongsTo('App\User', 'like_from');
    }

    public function likeTo()
    {
        return $this->belongsTo('App\User', 'like_to');
    }
}
