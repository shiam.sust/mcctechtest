<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/test', 'HomeController@test')->name('test');

Route::get('/ip', function () {
    $localIP = getHostByName(getHostName());
    $arr_ip = geoip()->getLocation("27.147.191.143");
    //return $arr_ip["lat"];
    dd($arr_ip);
    return $localIP;
});

Route::get('/register-req', 'AuthController@registration')->name('register-req');
Route::post('/register-action', 'AuthController@registrationAction')->name('register-action');

Route::get('/login-req', 'AuthController@login')->name('login-req');
Route::post('/login-action', 'AuthController@loginAction')->name('login-action');

Route::get('/logout-action', 'AuthController@logout')->name('logout-action');


Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/profile', 'HomeController@myProfile')->name('profile');
    Route::get('/user-list', 'HomeController@userList')->name('user-list');
    Route::post('/image-post', 'HomeController@postImage')->name('image-post');

    Route::post('/unlike', 'HomeController@unlikeUser')->name('unlike');
    Route::post('/like', 'HomeController@likeUser')->name('like');

});
