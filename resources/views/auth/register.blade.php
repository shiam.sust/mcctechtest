@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form id="registration_form" method="POST" action="{{ route('register-action') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-sm-4 col-form-label">Name</label>
                            <div class="col-sm-8">
                              <input type="text" class="form-control" id="name" name="name">
                              <span class="text-danger mt-0" id="errors_name" style="display: none;font-size: 12px">please enter your name</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label">Email</label>
                            <div class="col-sm-8">
                              <input type="text" class="form-control" id="email" name="email" placeholder="example@gmail.com">
                              <span class="text-danger mt-0" id="errors_email" style="display: none;font-size: 12px">please enter your email</span>
                              @if ($errors->has('email'))
                                            <span class="help-block text-danger">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-sm-4 col-form-label">Password</label>
                            <div class="col-sm-8">
                              <input type="password" class="form-control" id="password" name="password">
                              <span class="text-danger mt-0" id="errors_password" style="display: none;font-size: 12px">please enter your password</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="confirmPassword" class="col-sm-4 col-form-label">Confirm Password</label>
                            <div class="col-sm-8">
                              <input type="password" class="form-control" id="confirmPassword" name="password_confirmation">
                              <span class="text-danger mt-0" id="errors_confirmPassword" style="display: none;font-size: 12px">please enter your password again</span>
                              <span class="text-danger mt-0" id="errors_password_not_match" style="display: none;font-size: 12px">password not match</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="ip" class="col-sm-4 col-form-label">your ip address:</label>
                            <div class="col-sm-8">
                              <input type="text" class="form-control" name="ip" id="ip">
                              <div><span class="text-danger mt-0" id="errors_ip" style="display: none;font-size: 12px">please enter your ip address</span></div>
                              <a href="https://whatismyipaddress.com/" target="_blank">find your ip address</a>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="dob" class="col-sm-4 col-form-label">Date of Birth</label>
                            <div class="col-sm-8">
                              <input type="text" class="form-control datepickers" id="dob" name="dob" readonly>
                              <span class="text-danger mt-0" id="errors_dob" style="display: none;font-size: 12px">please enter your date of birth</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="gender" class="col-sm-4 col-form-label">Gender</label>
                            <div class="col-sm-8">
                                <select class="custom-select mr-sm-2" id="gender" name="gender">
                                    <option selected>Choose...</option>
                                    <option value="1">Male</option>
                                    <option value="2">Female</option>
                                    <option value="3">Other</option>
                                </select>
                                <span class="text-danger mt-0" id="errors_gender" style="display: none;font-size: 12px">please select your gender</span>
                            </div>
                        </div>

                        

                        
                    </form>
                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <a style="color: honeydew" id="submit" type="button" class="btn btn-primary">
                                Register
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
<script>
    $(document).ready(function() {

        $("#submit").click(function(){
            console.log("clicked");
            var flag = 0;

            if($('#name').val() == '' || $('#name').val() == null){
                flag = 1;
                $('#errors_name').show();
            }

            if($('#email').val() == '' || $('#email').val() == null){
                flag = 1;
                $('#errors_email').show();
            }

            if($('#password').val() == '' || $('#password').val() == null){
                flag = 1;
                $('#errors_password').show();
            }

            if($('#confirmPassword').val() == '' || $('#confirmPassword').val() == null){
                flag = 1;
                $('#errors_confirmPassword').show();
            }

            if($('#ip').val() == '' || $('#ip').val() == null){
                flag = 1;
                $('#errors_ip').show();
            }

            if($('#dob').val() == '' || $('#dob').val() == null){
                flag = 1;
                $('#errors_dob').show();
            }

            if($('#gender').val() == '' || $('#gender').val() == null){
                flag = 1;
                $('#errors_gender').show();
            }

            var pass1 = $('#password').val();
            var pass2 = $('#confirmPassword').val();

            if(pass1 != pass2){
                flag = 1;
                $('#errors_confirmPassword').hide();
                $('#errors_password_not_match').show(); 
            }

            if(flag == 0){
                if(confirm("Are you sure?")){
                    $("#registration_form").submit();
                }
            }
        });

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        }

        function showPosition(position) {
            console.log("Latitude: " + position.coords.latitude + 
            "<br>Longitude: " + position.coords.longitude);
        }

        $('.datepickers').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });

    });
</script>
@endsection
