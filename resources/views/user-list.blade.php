@extends('layouts.main')

@section('content')

<div class="container">
    <span class="text-center"><h3>List of users around 5 KM</h3></span>
    <div class="row justify-content-center">
        <div class="col-md-12 row">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Image</th>
                    <th scope="col">Distance</th>
                    <th scope="col">Gender</th>
                    <th scope="col">age</th>
                    <th scope="col">action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($users as $k => $user)
                        <tr>
                            <th scope="row">{{ $k+1 }}</th>
                            <td>{{ $user->name }}</td>
                            <td><img src="{{public_path('assets/user/'.$user->image)}}" style="height: 70px; width: 70px; onject-fit: cover" /></td>
                            <td>{{ round($distances[$user->id], 2) }} km</td>

                            @if($user->gender == 1)
                                <td>Male</td>
                            @elseif($user->gender == 2)
                                <td>Female</td>
                            @else 
                                <td>Other</td>
                            @endif

                            <td>{{ $ages[$user->id] }}</td>

                            @if($like_list[$user->id] == 1)
                                <td><button class="btn btn-danger" onclick="unlike({{$user->id}})">Unlike</button></td>
                            @else 
                                <td><button class="btn btn-success" onclick="like({{$user->id}})">Like</button></td>
                            @endif

                        </tr>
                    @endforeach
                </tbody>
            </table>    
            
        </div>
    </div>
</div>

@endsection

@section('custom_js')
<script>

    function unlike(user_id){
        //console.log("like");

        var ajaxurl = "{{route('unlike')}}";
            
        $.ajax({
            url: ajaxurl,
            type: "POST",
            data: {
                    '_token': "{{ csrf_token() }}",
                    'user_id': user_id
            },
            success: function(data){
                location.reload();
            },
        });
    }

    function like(user_id){
        //console.log("unlike");
        var ajaxurl = "{{route('like')}}";
            
        $.ajax({
            url: ajaxurl,
            type: "POST",
            data: {
                    '_token': "{{ csrf_token() }}",
                    'user_id': user_id
            },
            success: function(data){
                //console.log(data);
                if(data == 1){
                    alert("It's a match!");
                }
                location.reload();

            },
        });
    } 
    
</script>
@endsection
