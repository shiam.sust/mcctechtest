<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Validator;
use Auth;

class AuthController extends Controller
{
    public function registration()
    {
        return view('auth.register');
    }

    public function user_registration_rules(array $data)
    {
      $messages = [
        'email.unique' => 'email already exists!'
      ];

      $validator = Validator::make($data, [
            'email' => 'required|email|unique:users'
      ], $messages);

      return $validator;
    }

    public function registrationAction(Request $request)
    {
        //return $request;

        $validator = $this->user_registration_rules($request->toArray());
        if($validator->fails())
        {
          return redirect()->back()->withErrors($validator)->withInput();
        }

        $arr_ip = geoip()->getLocation($request->ip);
        $lat = $arr_ip["lat"];
        $lon = $arr_ip["lon"];

        $user =  new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->latitude = $lat;
        $user->longitude = $lon;
        $user->dob = $request->dob;
        $user->gender = $request->gender;

        $user->save();

        return Redirect::route('login-req')->with('success_message', 'you have successfully registered!');
    }

    public function login()
    {
        return view('auth.login');
    }

    public function loginAction(Request $request)
    {
        //return $request;
        if(!Auth::attempt($request->only(['email', 'password']) )){
            return redirect()->back()->with('error', 'Credentials do not match');
        }else{
            return Redirect::route('profile');
        }
    }

    public function logout()
    {
    	Auth::logout();
    	return redirect()->to(route('login-req'));
    }
}
