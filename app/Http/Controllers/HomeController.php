<?php

namespace App\Http\Controllers;

use App\Like;
use App\User;
use Illuminate\Http\Request;

use Auth;
use DateTime;
use Image;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function myProfile()
    {
        $user_id = Auth::user()->id;

        $my_profile = User::find($user_id);
        //return $my_profile;
        return view('profile')->with([
            'profile_info' => $my_profile
        ]);
    }

    public function postImage(Request $request)
    {
        //return $request;
        if(Auth::user()->id){
            //return $request;
            if($request->hasFile('img')){
                $extension = $request->img->extension();
                $name = time().rand(1000,9999).'.'.$extension;
                $image = Image::make($request->img);
                $image->save(public_path().'/assets/user/'.$name);
                //$path = $request->image->storeAs('products', $name);
            }
            if(isset($name)){
                $profiledata = [
                    'image' => $name
                ];
                $updateOrder = User::where('id', Auth::user()->id)->update($profiledata);
            }
        }

        return redirect()->back(); 
    }

    public function userList()
    {
        $my_id = Auth::user()->id;
        $users = User::where('id','!=',$my_id)->get();
        $me = User::where('id',$my_id)->first();

        $my_likes = Like::where('like_from', $my_id)->get();
        //return $my_likes;

        
        
        $target_users = [];
        $distances = [];
        $ages = [];
        $like_list = [];

        foreach($users as $user){
            $like_list[$user->id] = 0;
        }

        foreach($users as $user){
            $lat1 = $me->latitude;
            $lon1 = $me->longitude;
            $lat2 = $user->latitude;
            $lon2 = $user->longitude;
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $kms = $miles * 1.609344;

            if($kms <= 5){
                array_push($target_users, $user);
                //array_push($distances, $kms);
                $distances[$user->id] = $kms;

                $from = new DateTime($user->dob);
                $to   = new DateTime('today');
                $age = $from->diff($to)->y;
                $ages[$user->id] = $age;

                foreach($my_likes as $data){
                    if($data->like_to == $user->id){
                        $like_list[$user->id] = 1;
                    }
                }
            }

        }

        //return $like_list;
        //return ($miles * 1.609344);

        return view('user-list')->with([
            'users' => $target_users,
            'distances' => $distances,
            'ages' => $ages,
            'like_list' => $like_list
        ]);
    }

    public function unlikeUser(Request $request)
    {
        $user_id = $request->user_id;
        $my_id = Auth::user()->id;

        $like = Like::where('like_from', $my_id)->where('like_to', $user_id)->first();
        $like->delete();

        return;
    }

    public function likeUser(Request $request)
    {
        $my_id = Auth::user()->id;
        $user_id = $request->user_id;

        $like = new Like();
        $like->like_from = $my_id;
        $like->like_to = $user_id;
        $like->save();

        $opposite_like = Like::where('like_from', $user_id)->where('like_to', $my_id)->first();

        if(!$opposite_like){
            return 0;
        }else{
            return 1;
        }

    }

    public function test(Request $request)
    {
        $user_id = $request->user_id;
        $my_id = Auth::user()->id;

        $like = Like::where('like_from', 1)->where('like_to', 5)->first();
        // $like = Like::whereColumn([
        //     ['like_from', '=', 1],
        //     ['like_to', '=', 5],
        // ])->first();
        //$like->delete();

        return $like;
    }
}
