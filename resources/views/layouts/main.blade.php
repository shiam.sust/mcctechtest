<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>MCCTechTest</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" crossorigin="anonymous"></script>

    
    <link rel="stylesheet" type="text/css" href="{{Request::root()}}/assets/bootstrap-datepicker.css">
    <script src="{{Request::root()}}/assets/bootstrap-datepicker.js"></script>
    <script src="{{Request::root()}}/assets/jquery.min.js"></script>

</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a href="{{ route('profile')}}" class="navbar-brand">MCCTechTest</a>  		
        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div id="navbarCollapse" class="collapse navbar-collapse justify-content-start">
            @if(Auth::user())
                <div class="navbar-nav ml-auto action-buttons">
                    <div class="nav-item dropdown">
                        <a href="{{ route('profile')}}" class="nav-link mr-4">My profile</a>
                    </div>
                    <div class="nav-item dropdown">
                        <a href="{{ route('user-list')}}" class="nav-link mr-4">Users</a>
                    </div>
                    <div class="nav-item dropdown">
                        <a href="{{ route('logout-action') }}" class="btn btn-primary sign-up-btn">Logout</a>
                    </div>
                </div>
            @else 
                <div class="navbar-nav ml-auto action-buttons">
                    <div class="nav-item dropdown">
                        <a href="{{ route('login-req') }}" class="nav-link mr-4">Login</a>
                    </div>
                    <div class="nav-item dropdown">
                        <a href="{{ route('register-req') }}" class="btn btn-primary sign-up-btn">Registration</a>
                    </div>
                </div>
            @endif
            
            
        </div>
    </nav>

    <main class="py-4">
        @yield('content')
    </main>

    @yield('custom_js')
</body>
</html>
